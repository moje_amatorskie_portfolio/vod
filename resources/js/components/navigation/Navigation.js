import React from 'react';
import './style.css';
import '../global.css';
import './Navigation.scss';
import MainUserContext from '../main_user_context';
import reseedDatabaseWithMovies from '../functions/reseedDatabaseWithMovies';
import Cookies from 'js-cookie';
import getCookie from '../functions/getCookie';

const Navigation = ({links, MainSymflixSliderRef}) => {
    const clickRef = React.useRef();
    const [spinner, setSpinner] = React.useState(false);
    const handleClickReseed = (refreshAll) => {
        reseedDatabaseWithMovies();
        setSpinner(true);
        setTimeout(() => refreshAll(MainSymflixSliderRef), 1000);
        setTimeout(() => setSpinner(false), 1000);
    }
    return (<MainUserContext.Consumer>
        {
            (context) => (<div id="navigation" className='Navigation'>
                <nav>
                    <ul>
                        {
                            links.map((link, index) => <li key={index}>
                                {link.label}
                            </li>)
                        }
                        {(getCookie('isAdmin') == 'true') && <li className="text-warning font-weight-bold" ref={clickRef} onClick={() => handleClickReseed(context.refreshAll)}>RESEED</li>}
                    </ul>
                </nav>{
                    spinner
                        ? <div className="spinner-loader"></div>
                        : null
                }
            </div>)
        }
    </MainUserContext.Consumer>);
}

Navigation.defaultProps = {
    links: [
        {
            label: 'Specials',
            href: '/my-list'
        }, {
            label: 'Most watched',
            href: '/top'
        }, {
            label: 'Recent',
            href: '/top'
        }
    ]
}

export default Navigation;
