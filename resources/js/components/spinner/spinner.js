import React from 'react';
import './spinner.scss';

class Spinner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (<div className="spinner-loader"></div>)
    }
}

export default Spinner;
