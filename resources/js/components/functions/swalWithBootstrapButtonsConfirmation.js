import Swal from 'sweetalert2';
import deleteMovieFromDatabase from './deleteMovieFromDatabase';
import deleteMovieFromState from './deleteMovieFromState';

const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
})

const swalWithBootstrapButtonsConfirmation = (objectReference, movie_id, genre) => {
    swalWithBootstrapButtons.fire({
        title: 'Are you sure that you want to delete this movie?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            swalWithBootstrapButtons.fire(
                'Deleted!',
                'The movie has been deleted.',
                'success'
            );
            deleteMovieFromDatabase(movie_id);
            deleteMovieFromState(objectReference, movie_id, genre);
        } else if (
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Deletion of the movie has been cancelled',
                'error'
            );
        }
    })
}

export default swalWithBootstrapButtonsConfirmation;
