import React from 'react';

const deleteMovieFromState = (objectReference, movie_id, genre) => {
    objectReference.setState({
        ...objectReference.state,
        movies: {
            ...objectReference.state.movies,
            [genre]: objectReference.state.movies[genre].filter(function(singleMovieObject) {
                return singleMovieObject.id != movie_id
            })
        }
    });
}

export default deleteMovieFromState;
