import axios from 'axios';

const getMoviesFromDatabase = (objectReference, searchTerm = '') => {
    let formData = new FormData();
    formData.append('searchTerm', searchTerm);
    axios.post("/vod/public/api/movies", formData, {})
        .then(response => {
            // setTimeout(()=>{this.setState({retrieved:true})},1000); //delay because axios and setState are asynchronous and map doesn't work, because map() is called before data is retrieved from database and before it is stored in state uffff!!!
            //THE BEST AND GUARANTEED WAY OF RUNNING EACH SETSTATE CALL SEQUENTIALLY IS FUNCIONAL SETSTATE:
            objectReference.setState((state, props) => {
                return {
                    movies: response.data
                }
            });
            objectReference.setState((state, props) => {
                return {
                    retrieved: true
                }
            });
        })
        .catch(function(error) {
            console.log("Movie list retrieval error")
        })
        .finally(function() {
            //
        });
}

export default getMoviesFromDatabase;
