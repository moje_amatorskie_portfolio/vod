import axios from 'axios';

const deleteMovieFromDatabase = (movie_id) => {
    axios.post("/vod/public/api/delete/" + movie_id, {
            params: {}
        })
        .then(response => {})
        .catch(function(error) {})
        .finally(function() {});
}

export default deleteMovieFromDatabase;
