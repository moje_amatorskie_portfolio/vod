import getMoviesFromDatabase from './getMoviesFromDatabase';

const refreshAll = (objectReference) => {
    getMoviesFromDatabase(objectReference, '');
}

export default refreshAll;
