import refreshAll from './refreshAll';
import getMoviesFromDatabase from './getMoviesFromDatabase';
import swalSaveMovieConfirmation from './swalSaveMovieConfirmation';

const saveMovieToDatabase = (ct, objectReference) => {
    let movieIdToSave = '';
    if (ct.modalType == "Edit")
        movieIdToSave = ct.movieToEditOrAdd.id;
    let titleToSave = ct.titleForEdit;
    if (ct.modalType == "Add") {
        if (ct.titleForAdd)
            titleToSave = ct.titleForAdd;
        else titleToSave = 'Title';
    }
    let descriptionToSave = ct.textareaForEdit;
    if (ct.modalType == "Add") {
        if (ct.textareaForAdd) {
            descriptionToSave = ct.textareaForAdd;
        } else descriptionToSave = "Description";
    }
    let genreToSave = ct.genreToEditOrAdd;
    if (ct.genreSelected)
        genreToSave = ct.genreSelected;
    let imageToSave = ct.mainImageUploaded;
    let imageBgToSave = ct.detailsImageUploaded;
    let movieToSave = ct.movieUploaded;

    let formData = new FormData();
    formData.append('title', titleToSave);
    formData.append('description', descriptionToSave);
    formData.append('image', imageToSave);
    formData.append('imageBg', imageBgToSave);
    formData.append('movie', movieToSave);
    formData.append('genre', genreToSave);
    formData.append('id', movieIdToSave);

    axios.post('/vod/public/api/save',
            formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then(function(response) {
            refreshAll(objectReference);
            objectReference.setState({
                isSpinnerVisible: false
            });
            swalSaveMovieConfirmation();
        })
        .catch(function(error) {});
}

export default saveMovieToDatabase;
