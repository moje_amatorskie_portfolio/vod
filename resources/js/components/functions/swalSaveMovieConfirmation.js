import Swal from 'sweetalert2';

const swalSaveMovieConfirmation = () => {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'The movie has been saved',
        showConfirmButton: false,
        timer: 1500
    })
}

export default swalSaveMovieConfirmation;
