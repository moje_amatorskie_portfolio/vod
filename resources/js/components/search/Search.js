import React from 'react';
import '../global.css';
import '../App.css';

const Search = ({searchTerm, onKeyUp, onChange}) => (<div id="search" className='Search'>
    <input onKeyUp={onKeyUp} onChange={onChange} type="search" placeholder="Search for a title..." value={searchTerm}/>
</div>);

export default Search;
