import React from 'react';
import Logo from './logo';
import Navigation from './navigation';
import Search from './search';
import UserProfile from './user-profile';
import Hero from './hero';
import './global.css';
import './App.css';
import './navigation/style.css';
import MainSymflixSlider from './main-symflix-slider';
import EditOrAddMovieModal from './modals/EditOrAddMovieModal';
import UserMainViewContext from './user_main_view_context';
import MainUserContext from './main_user_context';
import getMoviesFromDatabase from './functions/getMoviesFromDatabase';

class UserMainView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            searchTerm: '',
            searchUrl: '',
            isSearch: false,
            headerBackgroundColor: '',
            showModal: false,
            genreToEditOrAdd: '',
            movieToEditOrAdd: '',
            modalType: '',
            showEditOrAddMovieModal: this.showEditOrAddMovieModal.bind(this),
            closeModal: this.closeModal.bind(this)
        };
        this.MainSymflixSliderRef = React.createRef();
        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.listenScrollEvent = this.listenScrollEvent.bind(this);
        this.showEditOrAddMovieModal = this.showEditOrAddMovieModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    listenScrollEvent(event) {
        if (window.scrollY < 100) {
            this.setState({headerBackgroundColor: ''});
        } else {
            this.setState({headerBackgroundColor: 'black'});
        }
    }
    componentDidMount() {
        window.addEventListener('scroll', this.listenScrollEvent);
    }
    handleKeyUp(event) {
        if (event.key === 'Enter' && this.state.searchTerm !== '') {
            let formData = new FormData();
            formData.append('searchTerm', this.state.searchTerm);
            setTimeout(() => getMoviesFromDatabase(this.MainSymflixSliderRef.current, this.state.searchTerm), 500);
        }
    }
    handleChange(event) {
        this.setState({searchTerm: event.target.value});
    }
    closeModal() {
        this.setState({showModal: false});
    }
    showEditOrAddMovieModal(movie, genre, type) {
        console.log('showEditOrAddMovieModal in user_main_view.js ' + type);
        this.setState({showModal: true});
        this.setState({genreToEditOrAdd: genre});
        this.setState({movieToEditOrAdd: movie});
        this.setState({modalType: type});
    }
    render() {
        return (<UserMainViewContext.Provider value={this.state}>
            <MainUserContext.Consumer>
                {
                    context => (<div>
                        <header className="Header" style={{
                                backgroundColor: this.state.headerBackgroundColor
                            }}>
                            <Logo className="Logo"/>
                            <Navigation className="Navigation" MainSymflixSliderRef={this.MainSymflixSliderRef}/>
                            <Search onKeyUp={this.handleKeyUp} onChange={this.handleChange} searchTerm={this.state.searchTerm} className="Search"/>
                            <UserProfile/>
                        </header>
                        <Hero className="Hero"/>
                        <MainSymflixSlider ref={this.MainSymflixSliderRef}/> {this.state.showModal && <EditOrAddMovieModal MainSymflixSliderObject={this.MainSymflixSliderRef.current}/>}
                    </div>)
                }
            </MainUserContext.Consumer>
        </UserMainViewContext.Provider>);
    }
}

export default UserMainView;
