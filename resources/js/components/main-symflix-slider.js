import React, {Component} from 'react';
import Slider from './SymflixSlider';
import './MainSymflixSlider.scss';
import axios from 'axios';
import deleteMovieFromDatabase from './functions/deleteMovieFromDatabase';
import swalWithBootstrapButtonsConfirmation from './functions/swalWithBootstrapButtonsConfirmation';
import MainUserContext from './main_user_context';
import getMoviesFromDatabase from './functions/getMoviesFromDatabase';

class MainSymflixSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            retrieved: false,
            movies: {},
            isSpinnerVisible: false
        }
        this.deleteMovie = this.deleteMovie.bind(this);
    }
    componentDidMount() {
        getMoviesFromDatabase(this);
    }
    deleteMovie(movie_id, genre) {
        swalWithBootstrapButtonsConfirmation(this, movie_id, genre);
    }
    render() {
        let allMoviesArray = [];
        for (const [genre, singleGenreMoviesArray] of Object.entries(this.state.movies)) {
            allMoviesArray.push({[genre]: singleGenreMoviesArray}); //change object into array for map();
        }
        return (<MainUserContext.Consumer>
            {
                (context) => (<div className="app">
                    {this.state.isSpinnerVisible && <div className="spinner-loader"></div>}
                    {
                        allMoviesArray.map((singleGenreMoviesObject, index) => (<div key={index}>
                            {
                                this.state.retrieved && <Slider genre={Object.keys(singleGenreMoviesObject)[0]} deleteMovie={this.deleteMovie}>
                                        {/* get first key from object */}
                                        {Object.values(singleGenreMoviesObject)[0].map(movie => (<Slider.Item movie={movie} genre={Object.keys(singleGenreMoviesObject)[0]} key={movie.id}></Slider.Item>))}
                                    </Slider>
                            }
                        </div>))
                    }
                </div>)
            }
        </MainUserContext.Consumer>);
    }
}

export default MainSymflixSlider;
