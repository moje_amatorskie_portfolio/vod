import React from 'react';
import HeroButton from '../hero-button';
import './style.css';
import '../global.css';
import pablo from './pablo.jpg';
import narcos from './narcos.png';

const Hero = ({ title, description }) => (
  <div
    className='Hero'
    style={{ backgroundImage: 'url("/vod/public/images/wojowniczka.webp")'}}>
    <div className='content'>
      <img
        className='logo'
        src={'/vod/public/images/poszukiwana1.png'}
        alt="narcos background"
      />
      <h2 className="subtitle">{title}</h2>
      <p>{description}</p>
      <div className='button-wrapper'>
        <HeroButton primary text="Watch now" />
        <HeroButton primary={false} text="+ My list" />
        <HeroButton primary={false} text="Recently Added" />
      </div>
    </div>
    <div className='overlay'></div>
  </div>
);

Hero.defaultProps = {
  title: 'Season 2 now available!',
  description: 'Po nieudanej próbie przeprawy przez Przełęcz Czerwonego Rogu Drużyna musiała dokonać wyboru dalszej drogi. Boromir zasugerował przeprawę przez Zielone Wrota.',
}

export default Hero;
