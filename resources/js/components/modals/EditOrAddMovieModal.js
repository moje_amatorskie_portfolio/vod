import React from 'react';
import saveMovieToDatabase from '../functions/saveMovieToDatabase';
import refreshAll from '../functions/refreshAll';
import swalSaveMovieConfirmation from '../functions/swalSaveMovieConfirmation';
import './modals.scss';
import UserMainViewContext from '../user_main_view_context';

class EditOrAddMovieModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalActive: true,
      genreSelected: '',
      titleForEdit: '',
      titleForAdd: '',
      textareaForEdit: '',
      textareaForAdd: '',
      mainImageUploaded: null,
      mainImageUploadedName: '',
      mainImageUploadedSrc: '',
      detailsImageUploaded: null,
      detailsImageUploadedName: '',
      detailsImageUploadedSrc: '',
      movieUploaded: null,
      movieUploadedName: '',
      movieUploadedSrc: ''
    }
    this.titleForEditRef = React.createRef();
    this.textareaRef = React.createRef();
    this.movieRef = React.createRef();
    this.movieTagRef = React.createRef();
    this.mainImageRef = React.createRef();
    this.mainImageTagRef = React.createRef();
    this.detailsImageRef = React.createRef();
    this.detailsImageTagRef = React.createRef();
    this.handleSave = this.handleSave.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.handleSelectBoxGenreChange = this.handleSelectBoxGenreChange.bind(this);
    this.handleTitleForEditChange = this.handleTitleForEditChange.bind(this);
    this.handleTitleForAddChange = this.handleTitleForAddChange.bind(this);
    this.handleTextareaChangeForEdit = this.handleTextareaChangeForEdit.bind(this);
    this.handleTextareaChangeForAdd = this.handleTextareaChangeForAdd.bind(this);
    this.handleMovieClick = this.handleMovieClick.bind(this);
    this.handleMovieChangeRef = this.handleMovieChangeRef.bind(this);
    this.handleMainImageClick = this.handleMainImageClick.bind(this);
    this.handleMainImageRef = this.handleMainImageRef.bind(this);
    this.handleDetailsImageClick = this.handleDetailsImageClick.bind(this);
    this.handleDetailsImageRef = this.handleDetailsImageRef.bind(this);
  }
  componentDidMount() {
    this.setState({titleForEdit: this.titleForEditRef.current.value});
    this.setState({textareaForEdit: this.textareaRef.current.value},()=>console.log("cDM:"+this.state.textareaForEdit));
  }
  handleSave(event, closeModal, movieToEditOrAdd, genreToEditOrAdd, modalType) {
    event.persist();
    closeModal();

    const context = {
      ...this.state,
      movieToEditOrAdd: movieToEditOrAdd,
      genreToEditOrAdd: genreToEditOrAdd,
      modalType: modalType
    }
    this.props.MainSymflixSliderObject.setState({isSpinnerVisible: true});
    saveMovieToDatabase(context,this.props.MainSymflixSliderObject);
  }
  toggleModal() {
    this.setState({isModalActive: !this.state.isModalActive});
  }
  handleSelectBoxGenreChange(event) {
    console.log(event.target.value);
    this.setState({genreSelected: event.target.value});
  }
  handleTitleForEditChange(event) {
    this.setState({titleForEdit: event.target.value});
  }
  handleTitleForAddChange(event) {
    this.setState({titleForAdd: event.target.value});
  }
  handleTextareaChangeForEdit(event) {
    this.setState({textareaForEdit: event.target.value})
    console.log("handleTextareaChangeForEdit")
  }
  handleTextareaChangeForAdd(event) {
    this.setState({textareaForAdd: event.target.value})
    console.log("handleTextareaChangeForAAdd")
  }
  handleMainImageClick(event) {
    this.mainImageRef.current.click();
  }
  handleMainImageRef(event) {
    event.persist(); //to use updater form of setState
    const mainImageUploaded = event.target.files[0];
    this.setState({mainImageUploadedSrc: URL.createObjectURL(event.target.files[0])},()=>this.mainImageTagRef.current.src= this.state.mainImageUploadedSrc);
    this.setState((state, props) => {
      return {
        mainImageUploaded: mainImageUploaded
      }
    })
    this.setState((state, props) => {
      return {
        mainImageUploadedName: mainImageUploaded.name
      }
    })
  }
  handleDetailsImageClick(event) {
    this.detailsImageRef.current.click();
  }
  handleDetailsImageRef(event) {
    event.persist();
    const detailsImageUploaded = event.target.files[0];
    this.setState({detailsImageUploadedSrc: URL.createObjectURL(event.target.files[0])}, () => this.detailsImageTagRef.current.src = this.state.detailsImageUploadedSrc);
    this.setState((state, props) => {
      return {
        detailsImageUploaded: detailsImageUploaded
      }
    })
    this.setState((state, props) => {
      return {
        detailsImageUploadedName: detailsImageUploaded.name
      }
    })
  }
  handleMovieClick(event) {
    this.movieRef.current.click();
  }
  handleMovieChangeRef(event) {
    event.persist();
    const movieUploaded = event.target.files[0];
    this.setState({movieUploadedSrc: URL.createObjectURL(event.target.files[0])}, () => this.movieTagRef.current.src = this.state.movieUploadedSrc);
    this.setState((state, props) => {
      return {
        movieUploaded: movieUploaded
      }
    })
    this.setState((state, props) => {
      return {
        movieUploadedName: movieUploaded.name
      }
    })
  }
  render() {
    return (
      <div>
        <UserMainViewContext.Consumer>
        {(context)=>(
        <>
        {this.state.isModalActive &&
          <>
          <div className="z-index-998"></div>
          <div className="z-index-999 border-yellow">
            <div className="flex-column border-green item-flex-grow-1">
              <div className="modal-title row-center border-red item-flex-grow-1">
                {context.modalType=="Edit" &&
                <>
                <div>You are editing</div>
                <div className="modal-title-uppercase">{this.state.titleForEdit}</div>

                </>}
                {context.modalType=="Add" &&
                <>
                <div>You are adding a new movie to genre </div>
                <div className="modal-title-uppercase">{this.state.genreSelected ? this.state.genreSelected : context.genreToEditOrAdd }</div>
                </>}
                <input hidden readOnly ref={this.titleForEditRef} value={context.movieToEditOrAdd.title} />
              </div>
              <div className="flex-row border-blue item-flex-grow-6">
                <div className="flex-column item-flex-grow-1 border-white">
                      <img className="modal-image  item-flex-grow-1" ref={this.mainImageTagRef} style={{borderRadius:"20px", height:"100%", maxHeight:"45vh", maxWidth:"100%"}}   src={context.modalType=='Edit' ? '/vod/public/images/'+context.movieToEditOrAdd.image : ''} alt="" />
                    <div className="row-space-evenly align-items-center">
                      <div onClick={this.handleMainImageClick} className="btn btn-outline-success mt-1"> {this.state.mainImageUploadedName ? this.state.mainImageUploadedName : 'Choose Main Image'}</div>
                        <input onChange={this.handleMainImageRef} ref={this.mainImageRef} className="btn btn-outline-success" type="file" accept="image/*" style={{display:'none'}} />
                    </div>
                </div>
                <div className="flex-column item-flex-grow-1 border-white">
                    <img className="modal-image  item-flex-grow-1" ref={this.detailsImageTagRef} style={{borderRadius:"20px", height:"100%", maxHeight:"45vh", maxWidth:"100%"}}   src={context.modalType=='Edit' ? '/vod/public/images/'+context.movieToEditOrAdd.imageBg : ''} alt="" />
                    <div className="row-space-evenly align-items-center">
                      <div onClick={this.handleDetailsImageClick} className="btn btn-outline-success mt-1"> {this.state.detailsImageUploadedName ? this.state.detailsImageUploadedName : 'Choose Details Image'}</div>
                      <input onChange={this.handleDetailsImageRef} ref={this.detailsImageRef} className="btn btn-outline-success" type="file" accept="image/*" style={{display:'none'}} />
                    </div>
                </div>
                <div className="flex-column item-flex-grow-1 ">
                    <video  ref={this.movieTagRef} className="modal-image item-flex-grow-1" style={{borderRadius:"20px", height:"100%", maxHeight:"45vh", maxWidth:"100%"}} controls width="100%" src={context.modalType=="Edit" ? '/vod/public/movies/'+context.movieToEditOrAdd.movie : ''}>
                    </video>
                  <div className="row-space-evenly align-items-center">
                    <div onClick={this.handleMovieClick} className="btn btn-outline-success mt-1"> {this.state.movieUploadedName ? this.state.movieUploadedName : 'Choose Movie'}</div>
                    <input onChange={this.handleMovieChangeRef} ref={this.movieRef} className="btn btn-outline-success" type="file" accept="video/mp4" style={{display:'none'}} />
                  </div>
                </div>
              </div>
              <div className="row-space-between border-orange item-flex-grow-2">
                <div className="flex-column column-space-evenly item-flex-grow-1 border-brown ">
                  <div className="row-space-between">
                    <div className="pl-5">Title</div>
                    {context.modalType=='Edit' &&
                    <div className="pr-5">
                      <input value={this.state.titleForEdit} className="text-white bg-black" onChange={()=>this.handleTitleForEditChange(event)}/>
                    </div>}
                    {context.modalType=='Add' &&
                    <div className="pr-5">
                      <input value={this.state.titleForAdd} className="text-warning bg-black" onChange={()=>this.handleTitleForAddChange(event)}/>
                    </div>}
                  </div>
                  <div className="row-space-between">
                    <div className="pl-5 mt-2">Genre</div>
                    {context.modalType=="Edit" &&
                    <div className="pr-5 mt-2 text-white">
                      <input disabled readOnly value={context.genreToEditOrAdd} className="text-white bg-black" />
                    </div>}
                    {context.modalType=="Add" &&
                    <div className="pr-5 mt-2">
                      <select className="bg-black text-white" onChange={this.handleSelectBoxGenreChange}>
                        <option value={context.genreToEditOrAdd}>{context.genreToEditOrAdd}</option>
                        <option value="Comedy">Comedy</option>
                        <option value="Action">Action</option>
                        <option value="Horror">Horror</option>
                        <option value="Thriller">Thriller</option>
                        <option value="Fantasy">Fantasy</option>
                        <option value="Love">Love</option>
                        <option value="Family">Family</option>
                      </select>
                    </div>}
                  </div>
                  <div className="flex-row">
                    <button className="btn btn-outline-danger item-flex-grow-1 ml-4 mr-4" onClick={context.closeModal}>
                      Cancel
                    </button>
                  </div>
                </div>
                <div className="flex-column column-space-evenly item-flex-grow-1 border-brown ">
                  <div className="row-space-between">
                    <div className="pl-3">Description</div>
                    <div className="row-space-evenly item-flex-grow-1">
                      <textarea hidden readOnly ref={this.textareaRef} value={context.movieToEditOrAdd.description}></textarea>
                      <textarea rows="3" value ={context.modalType=="Edit" ? this.state.textareaForEdit : this.state.textareaForAdd} className="modal-textarea text-white bg-black item-flex-grow-5 ml-2 mr-3" onChange={context.modalType=="Edit" ? this.handleTextareaChangeForEdit : this.handleTextareaChangeForAdd
                        }>
                      </textarea>
                    </div>
                  </div>
                  <div className="flex-row">
                    <button className="btn btn-outline-success item-flex-grow-1 ml-3 mr-1" onClick={(event)=>this.handleSave(event, context.closeModal, context.movieToEditOrAdd, context.genreToEditOrAdd, context.modalType)}>
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </>}
          </>
         )}
         </UserMainViewContext.Consumer>
      </div>
    )
  }
}

export default EditOrAddMovieModal
