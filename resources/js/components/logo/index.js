import React from 'react';
import './style.css';
import '../global.css';

const Logo = () => (<div className="Logo">
    <span>SYMFLIX VOD</span>
</div>);

export default Logo;
