import React from 'react';
import getCookie from '../functions/getCookie';
import './style.scss';
import '../global.css';

class UserProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedInUsername: "Not logged in"
        }
        this.clearLoggedInUsername = this.clearLoggedInUsername.bind(this);
        this.logout = this.logout.bind(this);
    }
    componentDidMount() {
        this.setState({loggedInUsername: getCookie('loggedInUsername')});
    }
    clearLoggedInUsername() {
        //document.cookie = "loggedInUsername=Not logged in"; overrides php cookie and php can't set cookie again
    }
    logout() {
        //document.cookie = "loggedInUsername=Zaloguj się;"; this overrides php cookie and stays set forever
        //document.cookie = "loggedInUsername=Log in"; not working  this.clearLoggedInUsername();
        //and overrides php cookie and php can't set so we delete cookie setting expiry date to a passed date :
        //document.cookie = "loggedInUsername=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"; this doesn't delete cookie as well and it slows down browser  when logging out
        var pathname = window.location.pathname;
        var basepath = pathname.substring(0, pathname.length - 21);
        window.location.replace(basepath + "/logout/manual");
    }
    render() {
        return (<div className="UserProfile">
            <div className="User">
                <div id="x" className="name">{this.state.loggedInUsername.replace('+', ' ')}
                </div>
                <i className="fa fa-cog fa-3x pl-2" aria-hidden="true"></i>
                <i className="fa fa-power-off fa-3x pl-2" aria-hidden="true" onClick={this.logout}></i>
            </div>
        </div>);
    }
}

export default UserProfile;
