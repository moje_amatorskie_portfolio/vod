import React, {useState} from 'react';
import ClassNames from 'classnames';
import SliderContext from './context';
import Content from './Content';
import SlideButton from './SlideButton';
import SliderWrapper from './SliderWrapper';
import useSliding from './useSliding';
import useSizeElement from './useSizeElement';
import Genre from './genre';
import './Slider.scss';
import getCookie from '../functions/getCookie';

const Slider = ({children, activeSlide, genre, deleteMovie}) => {
    const [currentSlide, setCurrentSlide] = useState(activeSlide);
    const {width, elementRef} = useSizeElement();
    const {
        handlePrev,
        handleNext,
        slideProps,
        containerRef,
        hasNext,
        hasPrev
    } = useSliding(width, React.Children.count(children));

    const handleSelect = movie => {

        setCurrentSlide(movie);
    };

    const handleClose = () => {
        console.log('handleSelect-onCloseSlide');
        setCurrentSlide(null);
    };
    const contextValue = {
        onSelectSlide: handleSelect,
        onCloseSlide: handleClose,
        elementRef,
        currentSlide,
        genre: genre,
        deleteMovie: deleteMovie
    };

    return (<SliderContext.Provider value={contextValue}>
        <SliderWrapper>
            <div className={ClassNames({
                    'slider-admin': getCookie('isAdmin') == 'true'
                }, {
                    'slider': getCookie('isAdmin') == 'false'
                }, {
                    'slider--open': currentSlide != null
                })}>
                <div ref={containerRef} className="slider__container" {...slideProps}>{children}</div>
            </div>
            {hasPrev && <SlideButton onClick={handlePrev} type="prev"/>}
            {hasNext && <SlideButton onClick={handleNext} type="next"/>}
        </SliderWrapper>
        {currentSlide && <Content movie={currentSlide} onClose={handleClose}/>}
        {/* display big slide */}
    </SliderContext.Provider>);
};

export default Slider;
