import React from 'react';
import IconArrowDown from './../Icons/IconArrowDown'
import './SlideButton.scss'
import ClassNames from 'classnames';
import getCookie from '../functions/getCookie';

const SlideButton = ({onClick, type}) => (<button className={getCookie('isAdmin') == 'false'
        ? `slide-button slide-button--${type}`
        : `slide-button-admin slide-button-admin--${type}`} onClick={onClick}>
    <span>
        <IconArrowDown/>
    </span>
</button>);

export default SlideButton;
