import React from 'react';
import IconCross from './../Icons/IconCross';
import './Content.scss';
import ClassNames from 'classnames';
import getCookie from '../functions/getCookie';

const Content = ({movie, onClose}) => (<div className={ClassNames({
        'content-admin': getCookie('isAdmin') == 'true'
    }, {
        'content': getCookie('isAdmin') == 'false'
    })} onClick={onClose}>
    <div className="content__background">
        <div className="content__background__shadow"/>
        <div className="content__background__image" style={{
                backgroundImage: `url(/vod/public/images/${movie.imageBg})`
            }}/>
    </div>
    <div className="content__area">
        <div className="content__area__container">
            <div className={getCookie('isAdmin') == 'true'
                    ? "content-admin__title"
                    : "content__title"}>{movie.title}</div>
            <div className={getCookie('isAdmin') == 'true'
                    ? "content-admin__description"
                    : "content__description"}>
                {movie.description}
            </div>
        </div>
        <button className={getCookie('isAdmin') == 'true'
                ? "content-admin__close"
                : "content__close"} onClick={onClose}>
            <IconCross/>
        </button>
    </div>
</div>);

export default Content;
