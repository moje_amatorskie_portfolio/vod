import React from 'react';
import ClassNames from 'classnames';
import getCookie from '../functions/getCookie';
import './AdminIconsContainer.scss';
import Swal from 'sweetalert2';
import swalWithBootstrapButtonsConfirmation from '../functions/swalWithBootstrapButtonsConfirmation';
import SliderContext from './context';
import UserMainViewContext from '../user_main_view_context';
import EditOrAddMovieModal from '../modals/EditOrAddMovieModal';
import '../modals/modals.scss';

class AdminIconsContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.swalEdit = this.swalEdit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }
    handleEdit(event, showEditOrAddMovieModal) {
        event.stopPropagation();
        showEditOrAddMovieModal(this.props.movie, this.props.genre, 'Edit');
        this.props.onCloseSlide();
    }
    handleAdd(event, showEditOrAddMovieModal) {
        event.stopPropagation();
        showEditOrAddMovieModal(this.props.movie, this.props.genre, 'Add')
        this.props.onCloseSlide();
    }
    handleDelete(event, deleteMovie) {
        event.stopPropagation();
        deleteMovie(this.props.movie.id, this.props.genre);
        this.props.onCloseSlide();
    }
    swalEdit() {
        Swal.fire({title: 'Error!', text: 'Do you want to continue', icon: 'error', confirmButtonText: 'Cool', isModalActive: false})
    }
    render() {
        return (<UserMainViewContext.Consumer>
            {
                (UserMainViewContext) => (<SliderContext.Consumer>
                    {
                        (SliderContext) => (<div>
                            {
                                getCookie('isAdmin') == 'true' && <> < div className = 'admin-icons-container' > <a className="btn  btn-outline-success mb-2 ml-1" aria-label="Add" onClick={(event) => {
                                            this.handleAdd(event, UserMainViewContext.showEditOrAddMovieModal)
                                        }}>
                                        <i className="fa fa-plus-square-o" aria-hidden="true"></i>
                                    </a>
                                    <a className="btn  btn-outline-primary mb-2 ml-1" aria-label="Edit" onClick={(event) => {
                                            this.handleEdit(event, UserMainViewContext.showEditOrAddMovieModal)
                                        }}>
                                        <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <a className="btn  btn-outline-danger mb-2 ml-1" aria-label="Delete" onClick={(event) => {
                                            this.handleDelete(event, SliderContext.deleteMovie)
                                        }}>
                                        <i className="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </>
                            }
                        </div>)
                    }
                </SliderContext.Consumer>)
            }
        </UserMainViewContext.Consumer>)
    }
}

export default AdminIconsContainer;
