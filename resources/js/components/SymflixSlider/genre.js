import React from 'react';
import './genre.scss';
import SliderContext from './context';
import getCookie from '../functions/getCookie'

class Genre extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (<SliderContext.Consumer>
            {
                contextValue => (<div className="h1 pl-5 ">
                    <span className={getCookie('isAdmin') == 'true'
                            ? 'text-warning'
                            : 'text-white'}>{contextValue.genre}</span>
                </div>)
            }
        </SliderContext.Consumer>)
    }
}

export default Genre;
