import React from 'react'
import './Mark.scss'
import ClassNames from 'classnames';
import getCookie from '../functions/getCookie';

const Mark = () => (<div className={ClassNames({
        'mark': getCookie('isAdmin') !== 'true'
    }, {
        'mark-admin': getCookie('isAdmin') == 'true'
    })}/>)

export default Mark;
