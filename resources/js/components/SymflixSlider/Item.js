import React from 'react';
import ClassNames from 'classnames';
import SliderContext from './context';
import ShowDetailsButton from './ShowDetailsButton';
import Mark from './Mark';
import getCookie from '../functions/getCookie';
import './Item.scss';
import AdminIconsContainer from './AdminIconsContainer';

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isPlayed: false
        }
        this.handlePlay = this.handlePlay.bind(this);
    }
    handlePlay(event) {
        event.stopPropagation();
        this.setState({isPlayed: true});
        console.log(this.state.isPlayed);
    }
    stopPropagation(event) {
        event.stopPropagation();
    }
    render() {
        return (<SliderContext.Consumer>
            {
                ({onSelectSlide, onCloseSlide, currentSlide, elementRef}) => {
                    var isActive = (currentSlide && (currentSlide.id === this.props.movie.id));
                    return (<div ref={elementRef} className={ClassNames('item', {'item--open': isActive})} onClick={() => onSelectSlide(this.props.movie)}>
                        {/* elementRef to div z jednym ze slajdów */}
                        <AdminIconsContainer genre={this.props.genre} movie={this.props.movie} onCloseSlide={onCloseSlide}/> {
                            !this.state.isPlayed && <> < img src = {
                                '/vod/public/images/' + this.props.movie.image
                            }
                            alt = "" /> <img className="play-icon" src={'/vod/public/images/play.png'} alt="" onClick={this.handlePlay}/>
                            </>
                        }
                        {
                            this.state.isPlayed && <video autoPlay={true} style={{
                                        borderRadius: "20px",
                                        height: "100%",
                                        maxHeight: "45vh",
                                        maxWidth: "100%",
                                        zIndex: "2222"
                                    }} controls="controls" width="100%" src={'/vod/public/movies/' + this.props.movie.movie} onClick={this.stopPropagation.bind(this)}></video>
                        }
                        {!this.state.isPlayed && <ShowDetailsButton onClick={() => onSelectSlide(this.props.movie)}/>}
                        {(isActive && !this.state.isPlayed) &&< Mark />}
                    </div>);
                }
            }
        </SliderContext.Consumer>)
    }
}

export default Item;
