import React from 'react';
import Genre from './genre';
import './SliderWrapper.scss';

const SliderWrapper = ({children}) => (<div className="slider-wrapper">
    <Genre/> {children}
</div>);

export default SliderWrapper;
