import React from 'react';
import PropTypes from 'prop-types';
import './style.css';
import '../global.css';

const HeroButton = ({text, primary}) => (<a href="#" className='Button' data-primary={primary}>
    {text}
</a>);

HeroButton.propTypes = {
    primary: PropTypes.bool,
    text: PropTypes.string
}

HeroButton.defaultProps = {
    text: '',
    primary: true
}

export default HeroButton;
