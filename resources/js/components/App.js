import React, {Component} from 'react';
import ReactDom from 'react-dom';
import {Route, Link, BrowserRouter, Switch} from 'react-router-dom';
import UserMainView from './user_main_view';
import Notfound from './notfound';
import MainUserContext from './main_user_context';
import getMoviesFromDatabase from './functions/getMoviesFromDatabase';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshAll: this.refreshAll.bind(this)
        }
        this.refreshAll = this.refreshAll.bind(this);
    }
    refreshAll(MainSymflixSliderRef) {
        getMoviesFromDatabase(MainSymflixSliderRef.current);
    }
    render() {
        return (<div>
            <MainUserContext.Provider value={this.state}>
                <BrowserRouter basename="/vod/public">
                    <div>
                        <Switch>
                            <Route exact="exact" path="/start/user-main-view" component={UserMainView}/>
                            <Route component={Notfound}/>
                        </Switch>
                    </div>
                </BrowserRouter>
            </MainUserContext.Provider>
        </div>);
    }
}

ReactDom.render(<App/>, document.getElementById('app'));
