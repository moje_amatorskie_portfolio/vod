<?php

use Illuminate\Database\Seeder;
use App\Movie;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 1;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 2;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 2;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 2;
        $movie->save();


        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 2;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 2;
        $movie->save();


        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 2;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 3;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;

        $movie->save();
        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 4;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 5;
        $movie->save();


        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "1983";
        $movie->description="1983 is a 2014 Indian Malayalam coming-of-age sports drama film directed by fashion photographer Abrid Shine, who has written the story and co-written the script with Bipin Chandran.";
        $movie->image = "slide1.jpg";
        $movie->imageBg = "slide1b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 6;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Russian Doll";
        $movie->description="Russian Dolls (French: Les Poupées russes) is a 2005 French-British comedy-drama film, the sequel to LAuberge Espagnole (2002) and the second part of the Spanish Apartment trilogy, which is concluded with Chinese Puzzle";
        $movie->image = "slide2.jpg";
        $movie->imageBg = "slide2b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "The Rain";
        $movie->description="The Rain is a Danish post-apocalyptic web television series created by Jannik Tai Mosholt, Esben Toft Jacobsen and Christian Potalivo.[1] It premiered on Netflix on 4 May 2018.";
        $movie->image = "slide3.jpg";
        $movie->imageBg = "slide3b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Sex Education";
        $movie->description="The film was released on 17 May 2019, with 6 episodes.[3] In June 2019, it was confirmed that the series was renewed for a third and final season.";
        $movie->image = "slide4.jpg";
        $movie->imageBg = "slide4b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Elite";
        $movie->description="Elite is a Spanish thriller teen drama web television series created for Netflix by Carlos Montero and Darío Madrona.";
        $movie->image = "slide5.jpg";
        $movie->imageBg = "slide5b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();

        $movie = new Movie();
        $movie->title = "Black Mirror";
        $movie->description="Black Mirror: Bandersnatch is a 2018 interactive film in the science fiction anthology series Black Mirror. It was written by series creator Charlie Brooker and directed by David Slade.";
        $movie->image = "slide6.jpg";
        $movie->imageBg = "slide6b.webp";
        $movie->movie = "oceans.mp4";
        $movie->genre_id = 7;
        $movie->save();
    }
}
