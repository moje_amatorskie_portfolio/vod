<?php

use Illuminate\Database\Seeder;
use App\Genre;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genre = new Genre();
        $genre->genre = "Comedy";
        $genre->save();
        $genre = new Genre();
        $genre->genre = "Action";
        $genre->save();
        $genre = new Genre();
        $genre->genre = "Horror";
        $genre->save();
        $genre = new Genre();
        $genre->genre = "Thriller";
        $genre->save();
        $genre = new Genre();
        $genre->genre = "Fantasy";
        $genre->save();
        $genre = new Genre();
        $genre->genre = "Love";
        $genre->save();
        $genre = new Genre();
        $genre->genre = "Family";
        $genre->save();
    }
}
