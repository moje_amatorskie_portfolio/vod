<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        'name'=>'Administrator',
        'email'=>'admin@admin.pl',
        'password'=>bcrypt('admin'),
        'is_admin'=>true,
      ]);
        User::create([
        'name'=>'Franek Nowak',
        'email'=>'franek@franek.pl',
        'password'=>bcrypt('franek'),
        'is_admin'=>false,
      ]);
        User::create([
        'name'=>'Marek Kowalski',
        'email'=>'marek@marek.pl',
        'password'=>bcrypt('marek'),
        'is_admin'=>false,
      ]);
    }
}
