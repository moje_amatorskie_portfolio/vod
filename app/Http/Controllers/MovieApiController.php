<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use App\Movie;
use Illuminate\Support\Facades\DB;

class MovieApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $genres = Genre::all(); //returns collection , not an array
        $searchTerm=$request->searchTerm;
        $jsonArray = array();
        foreach ($genres as $genre) {
            $singleGenreMoviesCollection = $genre->movies()->where('title', 'like', '%'.$searchTerm.'%')->get(); //this returns Collection, not array
            if ($singleGenreMoviesCollection->isNotEmpty()) {  //there are plenty of methods available for collections, which are wrapped arrays
                $jsonArray[$genre->genre] = $singleGenreMoviesCollection;
            }
        }
        return $jsonArray;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->id) {
            $movie = Movie::where('id', $request->id)->firstOrFail();
        } else {
            $movie = new Movie();
        }
        if ($request->file('image')) {
            $movie->image = $request->file('image')->store('', 'images');//movies is a disk defined in filesystems.php
        }
        if ($request->file('imageBg')) {
            $movie->imageBg = $request->file('imageBg')->store('', 'images');
        }
        if ($request->file('movie')) {
            $movie->movie = $request->file('movie')->store('', 'movies');
        }
        $movie->title = $request->title ?: '';
        $movie->description = $request->description ?: '';
        $movie->genre_id = Genre::where('genre', $request->genre)->firstOrFail()->id;

        $movie->save();
        return 'ok';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Regenerate and seed database
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function seed()
    {
        shell_exec('bash seed.sh');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $response = $movie->delete();
        if ($response) {
            return 'success';
        }
    }
}
