<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; //for Auth::logout() chyba

class LogoutController extends Controller
{
    /**
     * Where to redirect users after logoutt
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    public function manualLogout()
    {
        setcookie('loggedInUsername', 'not logged in'); //in here it doesn't work
        Auth::logout();
        return redirect('/home');
    }
}
