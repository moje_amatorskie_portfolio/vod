#!/bin/bash
browser-sync start --proxy "localhost/vod/public" --files "resources/views/*.* ,
public/js/app.js ,
resources/js/components/*.* ,
app/Http/Controllers/*.*,
routes/*.*,
resources/js/components/hero/*.*,
resources/js/components/hero-button/*.*,
resources/js/components/logo/*.*,
resources/js/components/navigation/*.*,
resources/js/components/search/*.*,
resources/js/components/context_api/*.*,
resources/js/components/user-profile/*.*,
resources/js/components/film-genre-list/*.*,
resources/js/components/film-sublist/*.*,
resources/js/components/functions/swalWithBootstrapButtonsConfirmation.js,
/var/www/html/vod/webpack.mix.js,
/var/www/html/vod/public/css/custom.css,
/var/www/html/vod/resources/views/layouts/*.*,
/var/www/html/vod/resources/views/*.*



"
