<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::match(['get','post'],'/movies','MovieApiController@index');
Route::match(['get','post'],'/delete/{movie}','MovieApiController@destroy');
Route::match(['get','post'],'/seed','MovieApiController@seed');
Route::match(['get','post'],'/save','MovieApiController@store');
